/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grupo2;

import java.util.Scanner;

/**
 *
 * @author 1181307
 */
public class Grupo2 {

    public static int GetInt(Object Message) {
        System.out.print(Message);
        return (new Scanner(System.in)).nextInt();
    }

    public static String GetString(Object Message) {
        System.out.print(Message);
        return (new Scanner(System.in)).next();
    }

    public static double GetDouble(Object Message) {
        System.out.print(Message);
        return (new Scanner(System.in)).nextDouble();
    }

    public static void PL(Object escrever) {
        System.out.println(escrever);
    }

    public static void P(Object escrever) {
        System.out.print(escrever);
    }

    public static Scanner scan = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here

        int exit = 0;
        while (exit != -1) {
            int salario = GetInt("Digite o seu salário: ");
            int idade = GetInt("Digite a sua idade: ");
            double descontosempresa;
            double descontosfunc;

            if (idade > 65) {
                if (salario >= 6000) {
                    descontosempresa = 6000 * 0.05;
                    descontosfunc = 6000 * 0.075;
                } else {
                    descontosempresa = salario * 0.05;
                    descontosfunc = salario * 0.075;
                }
            } else if (idade > 60) {
                if (salario >= 6000) {
                    descontosempresa = 6000 * 0.075;
                    descontosfunc = 6000 * 0.09;
                } else {
                    descontosempresa = salario * 0.075;
                    descontosfunc = salario * 0.09;
                }

            } else if (idade > 55) {
                if (salario >= 6000) {
                    descontosempresa = 6000 * 0.13;
                    descontosfunc = 6000 * 0.13;
                } else {
                    descontosempresa = salario * 0.13;
                    descontosfunc = salario * 0.13;
                }

            } else {
                if (salario >= 6000) {
                    descontosempresa = 6000 * 0.20;
                    descontosfunc = 6000 * 0.17;
                } else {
                    descontosempresa = salario * 0.20;
                    descontosfunc = salario * 0.17;
                }

            }
            PL("Descontos para a segurança social: " + descontosfunc + "\nDescontos para a empresa: " + descontosempresa + "\nSalário após os descontos: " + (salario - descontosempresa - descontosfunc));
            PL("Sair ?: S(-1) N(0)");
            exit = scan.nextInt();
        }
    }

}
