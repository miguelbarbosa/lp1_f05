/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package grupo4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author 1181307
 */
public class Grupo4 {
    public static int GetInt(Object Message){
        System.out.print(Message);
        return  (new Scanner(System.in)).nextInt();}

    public static String GetString(Object Message){
        System.out.print(Message);
        return  (new Scanner(System.in)).next();}

    public static double GetDouble(Object Message){
        System.out.print(Message);
        return  (new Scanner(System.in)).nextDouble();}

    public static void PL(Object escrever){ System.out.println(escrever);}

    public static void P(Object escrever){ System.out.print(escrever);}

    public static Scanner scan = new Scanner(System.in);

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws FileNotFoundException, IOException {
        // TODO code application logic here
        File ficheiro = new File("matrizes.txt");
        int [][] matriz1 = new int[6][6];
        int [][] matriz2 = new int[6][6];
        int [][] matrizr = new int[6][6];
        BufferedReader br = new BufferedReader(new FileReader(ficheiro));

        String st;
        String texto = "";
        while ((st = br.readLine()) != null) {
            texto = texto + st + "\n";

        }
        br.close();
        String [] linha = texto.split("\n");
        
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 6; j++) {
                String[] linhaM = linha[i*6+j].split("-");
                for (int k = 0; k < 6; k++) {
                    if (i==0) {
                        matriz1[j][k] = Integer.parseInt(linhaM[k]);
                    }
                    else {
                        matriz2[j][k] = Integer.parseInt(linhaM[k]);
                    }
                    
                }
            }
        }
        
        for (int i = 0; i < 2; i++) {
            
            PL("Matriz " + (i+1));
            PL("# | 0  1  2  3  4  5" );
            PL("------------------------");
            for (int j = 0; j < 6; j++) {
                P(j + " | ");
                
                for (int k = 0; k < 6; k++) {
                    if (i==0) {
                        P(matriz1[j][k] + "  ");
                    }
                    else {
                        P(matriz2[j][k] + "  ");
                    }
                }
                PL("");
            }
            PL("\n\n");
        }
        
        PL("Matrizes multiplicadas (1*2): ");
        PL("# | 0  1  2  3  4  5" );
        PL("------------------------");
        for (int i = 0; i < 6; i++) {
            for(int j = 0; j < 6; j++) {
                for(int k = 0; k < 6; k++) {
                    matrizr[i][j] += matriz1[i][k] * matriz2[k][j];
                }
            }
        }
        for (int i = 0; i < 6; i++) {
            P(i + " | ");
            for (int j = 0; j < 6; j++) {
                P(matrizr[i][j] + "  ");
            }
            PL("");
            
        }
        
        
    }
    
}
