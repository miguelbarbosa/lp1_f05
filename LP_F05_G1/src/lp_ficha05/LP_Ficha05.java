/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lp_ficha05;

import java.util.Scanner;

public class LP_Ficha05 {
    
    public static int GetInt(Object Message){
        System.out.print(Message);
        return  (new Scanner(System.in)).nextInt();}

    public static String GetString(Object Message){
        System.out.print(Message);
        return  (new Scanner(System.in)).next();}

    public static double GetDouble(Object Message){
        System.out.print(Message);
        return  (new Scanner(System.in)).nextDouble();}

    public static void PL(Object escrever){ System.out.println(escrever);}

    public static void P(Object escrever){ System.out.print(escrever);}

    public static Scanner scan = new Scanner(System.in);

    Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
       
        int c = 0;
        int inicio=0,fim=0;
        while (fim-inicio<=100) {
            inicio = GetInt("Digite o número inicial: ");
            fim = GetInt("Digite o número final. Nota: A sua diferença tem que ser superior a 100: ");
        } 
        
        for (int i = inicio; i <= fim; i++) {
            if (i % 12 == 0 ){
                System.out.println("");
            }
            if (i % 3 == 0 && i % 5 == 0 && i % 7 == 0) {
                System.out.print(" FEIRA.ISEP.IPP ");
            } else if (i % 3 == 0 && i % 5 == 0) {
                System.out.print(" FEIRA.ISEP ");
            } else if (i % 3 == 0 && i % 7 == 0) {
                System.out.print(" FEIRA.IPP ");
            } else if (i % 5 == 0 && i % 7 == 0) {
                System.out.print(" ISEP.IPP ");
            } else if (i % 3 == 0){
                 System.out.print(" FEIRA ");
            } else if (i % 5 == 0){
                 System.out.print(" ISEP ");
            }else if (i % 7 == 0){
                 System.out.print(" IPP ");
            }else System.out.print(" " + i +" ");
        }
    }

}
